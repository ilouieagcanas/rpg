﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;
using System.IO;

public class Builder : MonoBehaviour
{
    // private const string BUILD_PATH = "./Builds/";
    static string[] SCENES = FindEnabledEditorScenes();

    [MenuItem("Build Tool/Build Android")]
    static void BuildAndroid()
    {
        DoGenericConfigurations();

        // Switch platform
        BuildTargetGroup targetGroup = BuildTargetGroup.Android;
        BuildTarget target = BuildTarget.Android;

        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

        BuildPlayerOptions playerOptions = new BuildPlayerOptions();
        playerOptions.locationPathName = Path.GetFullPath( "." ) + "/Builds/RPG.apk";
        playerOptions.target = target;

        CreateBuild( playerOptions, targetGroup );
    }

    static void BuildiOS()
    {
        DoGenericConfigurations();

        // Switch platform
    }

    static void DoGenericConfigurations()
    {
        // Specify bundle ID
        // Specify keystore 
        // Specify keystore password
        // Specify alias
        // Specify alias password
    }

    private static void CreateBuild( BuildPlayerOptions p_playerOptions, BuildTargetGroup p_targetGroup )
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget( p_targetGroup, p_playerOptions.target );

        p_playerOptions.scenes = SCENES;

        BuildReport report = BuildPipeline.BuildPlayer( p_playerOptions );
        BuildSummary summary = report.summary;

        switch( summary.result )
        {
            case BuildResult.Cancelled:
            default:
                Debug.LogWarning( "Build has been cancelled!" );
                break;
            case BuildResult.Failed:
                Debug.LogError( "Build failed with " + summary.totalErrors + " errors" );

                BuildStep[] steps = report.steps;

                foreach( BuildStep step in steps )
                {
                    Debug.LogError( "STEP NAME:" + step.name );

                    foreach( BuildStepMessage message in step.messages )
                    {
                        Debug.LogError( "STEP MESSAGE:" + message.content );
                    }
                }
                break;
            case BuildResult.Succeeded:
                Debug.Log( "Build success!" );
                break;
        }
    }

    private static string[] FindEnabledEditorScenes() 
    {
        List<string> EditorScenes = new List<string>();

        foreach( EditorBuildSettingsScene scene in EditorBuildSettings.scenes )
        {
            if ( !scene.enabled ) continue;
            Debug.Log( "Scene: " + scene.path );
            EditorScenes.Add( scene.path );
        }

        return EditorScenes.ToArray();
    }
}
