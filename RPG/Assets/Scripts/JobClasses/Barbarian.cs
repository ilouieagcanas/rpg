public class Barbarian : BaseJob
{
    public Barbarian()
    {
        // JobType = JobType.Barbarian;
        Name = "Barbarian";
        SkillBuilder = new BarbarianSkillBuilder();
        Skills = SkillBuilder.BuildSkills();
    }
}