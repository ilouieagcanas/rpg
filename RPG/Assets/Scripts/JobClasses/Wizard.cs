public class Wizard : BaseJob
{
    public Wizard()
    {
        // JobType = JobType.Wizard;
        Name = "Wizard";
        SkillBuilder = new WizardSkillBuilder();
        Skills = SkillBuilder.BuildSkills();
    }
}