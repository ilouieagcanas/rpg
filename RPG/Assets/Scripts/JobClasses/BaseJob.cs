using System.Collections.Generic;

public class BaseJob : IJob
{
    // public JobType JobType { get; protected set; }
    public string Name { get; protected set; }
    public ISkillBuilder SkillBuilder { get; protected set; }

    public List<ISkill> Skills { get; protected set; } = new List<ISkill>();

    public virtual bool UseSkill( int p_skillIndex )
    {
        ISkill skill = Skills[p_skillIndex];

        if( skill != null )
        {
            skill.Cast();
            return true;
        }

        return false;
    }
}