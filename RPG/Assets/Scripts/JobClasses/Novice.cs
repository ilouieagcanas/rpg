﻿public class Novice : BaseJob
{
    public Novice()
    {
        // JobType = JobType.Novice;
        Name = "Novice";
        SkillBuilder = new NoviceSkillBuilder();
        Skills = SkillBuilder.BuildSkills();
    }
}
