using System.Collections.Generic;
public interface IJob
{
    // JobType JobType { get; }
    string Name { get; }
    ISkillBuilder SkillBuilder { get; }
    List<ISkill> Skills { get; }

    bool UseSkill( int p_skillIndex );
}

// public enum JobType
// {
//     None        = -1,
//     Novice      = 0,
//     Paladin     = 1,
//     Cleric      = 2,
//     Ranger      = 3,
//     Barbarian   = 4,
//     Wizard      = 5,
// }