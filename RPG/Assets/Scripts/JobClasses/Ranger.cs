public class Ranger : BaseJob
{
    public Ranger()
    {
        // JobType = JobType.Ranger;
        Name = "Ranger";
        SkillBuilder = new RangerSkillBuilder();
        Skills = SkillBuilder.BuildSkills();
    }
}