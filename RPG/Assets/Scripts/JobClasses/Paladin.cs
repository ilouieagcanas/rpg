public class Paladin : BaseJob
{
    public Paladin()
    {
        // JobType = JobType.Paladin;
        Name = "Paladin";
        SkillBuilder = new PaladinSkillBuilder();
        Skills = SkillBuilder.BuildSkills();
    }
}