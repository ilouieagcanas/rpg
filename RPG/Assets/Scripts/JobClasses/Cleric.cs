public class Cleric : BaseJob
{
    public Cleric()
    {
        // JobType = JobType.Cleric;
        Name = "Cleric";
        SkillBuilder = new ClericSkillBuilder();
        Skills = SkillBuilder.BuildSkills();
    }
}