using System.Collections.Generic;
public interface IInventory
{
    Dictionary<ItemSlot, IItem> Items { get; }
    void EquipItem( IItem p_item );
    IItem UnequipItem( ItemSlot p_handSlot );

    IItem GetItem( ItemSlot p_handSlot );
}