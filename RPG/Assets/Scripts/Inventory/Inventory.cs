using System.Collections.Generic;
using UnityEngine;
public class Inventory : IInventory
{
    public Dictionary<ItemSlot, IItem> Items { get; } = new Dictionary<ItemSlot, IItem>();

    public void EquipItem( IItem p_item )
    {
        if( Items.ContainsKey( p_item.ItemSlot ) )
        {
            Items[p_item.ItemSlot] = p_item;
        }
        else
        {
            Items.Add( p_item.ItemSlot, p_item );
        }

        Debug.Log( $"Equipped item with type { p_item.ItemType } on { p_item.ItemSlot }." );
    }

    public IItem GetItem( ItemSlot p_handSlot )
    {
        if( Items.ContainsKey( p_handSlot ) )
        {
            return Items[p_handSlot];
        }

        return null;
    }

    public IItem UnequipItem( ItemSlot p_handSlot )
    {
        if( Items.ContainsKey( p_handSlot ) )
        {
            IItem item = Items[p_handSlot];
            Debug.Log( $"Unequipped item with type { item.ItemType } on { p_handSlot }" );
            Items.Remove( p_handSlot );
            return item;
        }

        return null;
    }
}