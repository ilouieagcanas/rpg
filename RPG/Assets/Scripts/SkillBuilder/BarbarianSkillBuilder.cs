using System.Collections.Generic;
public class BarbarianSkillBuilder : ISkillBuilder
{
    public List<ISkill> BuildSkills()
    {
        List<ISkill> skills = new List<ISkill>();
        skills.Add( new BleedingStrike() );
        skills.Add( new Cripple() );
        skills.Add( new Counter() );

        return skills;
    }
}