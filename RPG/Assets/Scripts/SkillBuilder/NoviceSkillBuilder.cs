using System.Collections.Generic;

public class NoviceSkillBuilder : ISkillBuilder
{
    public List<ISkill> BuildSkills()
    {
        List<ISkill> skills = new List<ISkill>();
        skills.Add( new ProtectiveShock() );
        skills.Add( new Counter() );

        return skills;
    }
}