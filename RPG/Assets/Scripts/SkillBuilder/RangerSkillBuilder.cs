using System.Collections.Generic;
public class RangerSkillBuilder : ISkillBuilder
{
    public List<ISkill> BuildSkills()
    {
        List<ISkill> skills = new List<ISkill>();
        skills.Add( new SplinterShot() );
        skills.Add( new PiercingRain() );
        skills.Add( new Cripple() );
        skills.Add( new Counter() );

        return skills;
    }
}