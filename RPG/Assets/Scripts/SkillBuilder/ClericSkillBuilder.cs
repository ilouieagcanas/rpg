using System.Collections.Generic;

public class ClericSkillBuilder : ISkillBuilder
{
    public List<ISkill> BuildSkills()
    {
        List<ISkill> skills = new List<ISkill>();
        skills.Add( new HealingWard() );
        skills.Add( new HolyCheck() );
        skills.Add( new ArcaneBurst() );
        skills.Add( new Counter() );

        return skills;
    }
}