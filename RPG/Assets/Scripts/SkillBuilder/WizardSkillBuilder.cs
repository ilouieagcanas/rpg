using System.Collections.Generic;
public class WizardSkillBuilder : ISkillBuilder
{
    public List<ISkill> BuildSkills()
    {
        List<ISkill> skills = new List<ISkill>();
        skills.Add( new SunfireNova() );
        skills.Add( new ChaosBlitz() );
        skills.Add( new ArcaneBurst() );
        skills.Add( new Counter() );

        return skills;
    }
}