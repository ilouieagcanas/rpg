using System.Collections.Generic;
public interface ISkillBuilder
{
    List<ISkill> BuildSkills();
}