using System.Collections.Generic;
public class PaladinSkillBuilder : ISkillBuilder
{
    public List<ISkill> BuildSkills()
    {
        List<ISkill> skills = new List<ISkill>();
        skills.Add( new DivineJustice() );
        skills.Add( new WhirlingThrust() );
        skills.Add( new HolyCheck() );
        skills.Add( new Counter() );

        return skills;
    }
}