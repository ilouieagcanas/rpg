public interface ISkill
{
    string Name { get; }
    void Cast();
}