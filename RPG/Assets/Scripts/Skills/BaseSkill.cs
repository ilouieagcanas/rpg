using UnityEngine;
public abstract class BaseSkill : ISkill
{
    public string Name { get; protected set; }

    public virtual void Cast()
    {
        Debug.Log( "Spell Casted: " + Name );
    }
}