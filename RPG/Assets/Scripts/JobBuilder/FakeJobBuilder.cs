public class FakeJobBuilder : IJobBuilder
{
    private IJob _job;

    public FakeJobBuilder( IJob p_job )
    {
        _job = p_job;
    }

    public IJob BuildJob( ItemType p_itemCombination )
    {
        return _job;
    }
}