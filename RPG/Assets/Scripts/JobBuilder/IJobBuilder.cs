public interface IJobBuilder
{
    IJob BuildJob( ItemType p_itemCombination );
}