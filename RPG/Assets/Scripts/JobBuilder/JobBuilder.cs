public class JobBuilder : IJobBuilder
{
    public IJob BuildJob( ItemType p_itemCombination )
    {
        if( p_itemCombination == ItemType.None ) { return null; }

        switch( p_itemCombination )
        {
            case JobItemCombination.PALADIN:      return new Paladin();
            case JobItemCombination.CLERIC:       return new Cleric();
            case JobItemCombination.RANGER:       return new Ranger();
            case JobItemCombination.BARBARIAN:    return new Barbarian();
            case JobItemCombination.WIZARD:       return new Wizard();
            default:                              return new Novice();
        }
    }
}