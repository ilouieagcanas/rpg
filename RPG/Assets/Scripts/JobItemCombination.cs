public static class JobItemCombination
{
    public const ItemType PALADIN      = ItemType.Shield | ItemType.Sword; 
    public const ItemType CLERIC       = ItemType.Shield | ItemType.Staff;
    public const ItemType RANGER       = ItemType.Bow | ItemType.Dagger;
    public const ItemType BARBARIAN    = ItemType.Axe;
    public const ItemType WIZARD       = ItemType.Spellbook | ItemType.Staff;
}