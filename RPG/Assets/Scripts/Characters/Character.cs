﻿using System.Collections.Generic;
using UnityEngine;
public class Character : ICharacter
{
    public IInventory Inventory { get; }
    private IJobBuilder _jobBuilder; 

    public IJob Job { get; private set; }

    public Character( IInventory p_inventory, IJobBuilder p_jobBuilder )
    {
        Inventory = p_inventory;
        _jobBuilder = p_jobBuilder;
    }

    public void EquipItem( IItem p_item )
    {
        Inventory.EquipItem( p_item );
    }

    public IItem UnequipItem( ItemSlot p_handSlot )
    {
        return Inventory.UnequipItem( p_handSlot );
    }

    public IItem GetItem( ItemSlot p_handSlot )
    {
        return Inventory.GetItem( p_handSlot );
    }

    public void BuildJob()
    {
        // Get left hand item type
        IItem leftHandItem = GetItem( ItemSlot.LeftHand );
        ItemType leftHandType = ( leftHandItem != null ) ? leftHandItem.ItemType : ItemType.None;

        // Get right hand item type
        IItem rightHandItem = GetItem( ItemSlot.RightHand );
        ItemType rightHandType = ( rightHandItem != null ) ? rightHandItem.ItemType : ItemType.None;

        Job = _jobBuilder.BuildJob( leftHandType | rightHandType );

        if( Job != null )
        {
            Debug.Log( $"Changed job to { Job.Name }" );
        }
        else
        {
            Debug.Log( $"You are now jobless!" );
        }
    }

    public bool UseSkill( int p_skillIndex )
    {
        if( Job == null) { return false; }

        return Job.UseSkill( p_skillIndex );
    }
}
