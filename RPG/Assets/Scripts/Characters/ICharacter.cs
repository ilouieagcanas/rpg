﻿using System.Collections.Generic;

public interface ICharacter
{
    IInventory Inventory { get; }
    IJob Job { get; }
    void EquipItem( IItem p_item );
    IItem UnequipItem( ItemSlot p_handSlot );
    IItem GetItem( ItemSlot p_handSlot );

    void BuildJob();
    bool UseSkill( int p_skillIndex );
}
