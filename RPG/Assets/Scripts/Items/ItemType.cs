using System;

[Flags]
public enum ItemType
{
    None        = 0,
    Shield      = 1,
    Sword       = 2,
    Dagger      = 4,
    Axe         = 8,
    Staff       = 16,
    Spellbook   = 32,
    Bow         = 64,
}
