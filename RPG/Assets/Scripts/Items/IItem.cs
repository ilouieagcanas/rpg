using System;
public interface IItem
{
    string Name { get; }
    ItemSlot ItemSlot { get; }
    ItemType ItemType { get; }
}
