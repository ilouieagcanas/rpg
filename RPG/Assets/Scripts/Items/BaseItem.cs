﻿public abstract class BaseItem : IItem
{
    public string Name { get; protected set; }
    public ItemSlot ItemSlot { get; protected set; }
    public ItemType ItemType { get; protected set; }
}