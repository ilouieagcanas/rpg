public class Bow : BaseItem
{
    public Bow()
    {
        Name = "Bow";
        ItemSlot = ItemSlot.LeftHand;
        ItemType = ItemType.Bow;
    }
}