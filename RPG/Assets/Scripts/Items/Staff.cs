public class Staff : BaseItem
{
    public Staff()
    {
        Name = "Staff";
        ItemSlot = ItemSlot.RightHand;
        ItemType = ItemType.Staff;
    }
}