public class Dagger : BaseItem
{
    public Dagger()
    {
        Name = "Dagger";
        ItemSlot = ItemSlot.RightHand;
        ItemType = ItemType.Dagger;
    }
}