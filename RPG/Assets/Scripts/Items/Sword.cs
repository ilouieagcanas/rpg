public class Sword : BaseItem
{
    public Sword()
    {
        Name = "Sword";
        ItemSlot = ItemSlot.RightHand;
        ItemType = ItemType.Sword;
    }
}