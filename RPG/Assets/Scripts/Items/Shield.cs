public class Shield : BaseItem
{
    public Shield()
    {
        Name = "Shield";
        ItemSlot = ItemSlot.LeftHand;
        ItemType = ItemType.Shield;
    }
}