public class Axe : BaseItem
{
    public Axe()
    {
        Name = "Axe";
        ItemSlot = ItemSlot.RightHand;
        ItemType = ItemType.Axe;
    }
}