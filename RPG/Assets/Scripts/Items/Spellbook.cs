public class Spellbook : BaseItem
{
    public Spellbook()
    {
        Name = "Spellbook";
        ItemSlot = ItemSlot.LeftHand;
        ItemType = ItemType.Spellbook;
    }
}