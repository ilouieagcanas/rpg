public class FakeItem : IItem
{
    public string Name { get; set; } = "Fake Item";
    public ItemSlot ItemSlot { get; set; }
    public ItemType ItemType { get; set; }
}