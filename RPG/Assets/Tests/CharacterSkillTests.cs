﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class CharacterSkillTests
    {
        [Test]
        public void UseSkill_WithoutJob_ReturnsFalse()
        {
            Character character = new Character( new Inventory(), new FakeJobBuilder( null ) );
            character.BuildJob();

            bool bDidCast = character.UseSkill( 0 );

            Assert.IsFalse( bDidCast ); 
        }

        [Test]
        public void UseSkill_NoviceSkills_ReturnsTrue()
        {
            Character character = new Character( new Inventory(), new FakeJobBuilder( new Novice() ) );
            character.BuildJob();

            bool bDidCastAll = false;

            for( int i = 0; i < character.Job.Skills.Count; i++ )
            {
                bDidCastAll = character.UseSkill( i );

                if( !bDidCastAll ) { break; }
            }

            Assert.IsTrue( bDidCastAll ); 
        }

        [Test]
        public void UseSkill_PaladinSkills_ReturnsTrue()
        {
            Character character = new Character( new Inventory(), new FakeJobBuilder( new Paladin() ) );
            character.BuildJob();

            bool bDidCastAll = false;

            for( int i = 0; i < character.Job.Skills.Count; i++ )
            {
                bDidCastAll = character.UseSkill( i );

                if( !bDidCastAll ) { break; }
            }

            Assert.IsTrue( bDidCastAll ); 
        }

        [Test]
        public void UseSkill_ClericSkills_ReturnsTrue()
        {
            Character character = new Character( new Inventory(), new FakeJobBuilder( new Cleric() ) );
            character.BuildJob();

            bool bDidCastAll = false;

            for( int i = 0; i < character.Job.Skills.Count; i++ )
            {
                bDidCastAll = character.UseSkill( i );

                if( !bDidCastAll ) { break; }
            }

            Assert.IsTrue( bDidCastAll ); 
        }

        [Test]
        public void UseSkill_RangerSkills_ReturnsTrue()
        {
            Character character = new Character( new Inventory(), new FakeJobBuilder( new Ranger() ) );
            character.BuildJob();

            bool bDidCastAll = false;

            for( int i = 0; i < character.Job.Skills.Count; i++ )
            {
                bDidCastAll = character.UseSkill( i );

                if( !bDidCastAll ) { break; }
            }

            Assert.IsTrue( bDidCastAll ); 
        }

        [Test]
        public void UseSkill_BarbarianSkills_ReturnsTrue()
        {
            Character character = new Character( new Inventory(), new FakeJobBuilder( new Barbarian() ) );
            character.BuildJob();

            bool bDidCastAll = false;

            for( int i = 0; i < character.Job.Skills.Count; i++ )
            {
                bDidCastAll = character.UseSkill( i );

                if( !bDidCastAll ) { break; }
            }

            Assert.IsTrue( bDidCastAll ); 
        }

        [Test]
        public void UseSkill_WizardSkills_ReturnsTrue()
        {
            Character character = new Character( new Inventory(), new FakeJobBuilder( new Wizard() ) );
            character.BuildJob();

            bool bDidCastAll = false;

            for( int i = 0; i < character.Job.Skills.Count; i++ )
            {
                bDidCastAll = character.UseSkill( i );

                if( !bDidCastAll ) { break; }
            }

            Assert.IsTrue( bDidCastAll ); 
        }
    }
}
