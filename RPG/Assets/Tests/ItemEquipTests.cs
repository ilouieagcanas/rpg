﻿using NUnit.Framework;
using UnityEngine.TestTools;

namespace Tests
{
    public class ItemEquipTests
    {
        [TestCase( ItemSlot.LeftHand )]
        [TestCase( ItemSlot.RightHand )]
        public void EquipItem_OnSlot_ReturnsSameItem( ItemSlot p_slot )
        {
            //Arrange
            ICharacter character = new Character( new Inventory(), new FakeJobBuilder( null ) );
            IItem mockItem = new FakeItem { ItemSlot = p_slot };

            //Act
            character.EquipItem( mockItem );

            //Assert
            Assert.AreSame( mockItem, character.GetItem( p_slot ) );
        }

        [TestCase( ItemSlot.LeftHand )]
        [TestCase( ItemSlot.RightHand )]
        public void EquipItem_OnSlotWithExistingItem_ReturnsSameItem( ItemSlot p_itemSlot )
        {
            ICharacter character = new Character( new Inventory(), new FakeJobBuilder( null ) );
            IItem stubItem = new FakeItem { Name = "FakeItem1", ItemSlot = p_itemSlot };
            // character.EquipItem( stubItem, p_itemSlot );
            character.EquipItem( stubItem );

            IItem mockItem = new FakeItem { Name = "FakeItem2", ItemSlot = p_itemSlot };
            // character.EquipItem( mockItem, p_itemSlot );
            character.EquipItem( mockItem );

            Assert.AreSame( mockItem, character.GetItem( p_itemSlot ) );
        }

        [TestCase( ItemSlot.LeftHand, ItemSlot.RightHand )]
        [TestCase( ItemSlot.RightHand, ItemSlot.LeftHand )]
        public void EquipItem_OnDifferentSlotsAtTheSameTime_ReturnsSameNumberOfItems( ItemSlot p_slot1, ItemSlot p_slot2 )
        {
            ICharacter character = new Character( new Inventory(), new FakeJobBuilder( null ) );
            IItem stubItem = new FakeItem { ItemSlot = p_slot1 };
            character.EquipItem( stubItem );

            IItem stubItem2 = new FakeItem { ItemSlot = p_slot2 };
            character.EquipItem( stubItem2 );

            Assert.AreEqual( 2, character.Inventory.Items.Count );
        }

        // [TestCase( ItemSlot.LeftHand )]
        // [TestCase( ItemSlot.RightHand )]
        // public void EquipItem_WithAnyHandItemSlotOnOnAnyHand_ReturnsSameItem( ItemSlot p_handSlot )
        // {
        //     ICharacter character = new Character( new Inventory(), new FakeJobBuilder( null ) );
        //     IItem mockItem = new FakeItem { ItemSlot = ItemSlot.AnyHand };
        //     character.EquipItem( mockItem, p_handSlot );

        //     Assert.AreEqual( mockItem, character.GetItem( p_handSlot ) );
        // }

        [TestCase( ItemSlot.LeftHand )]
        [TestCase( ItemSlot.RightHand )]
        public void UnqeuipItem_ItemOnSlot_ReturnsSameItem( ItemSlot p_handSlot )
        {
            ICharacter character = new Character( new Inventory(), new FakeJobBuilder( null ) );
            IItem mockItem = new FakeItem { ItemSlot = p_handSlot };
            character.EquipItem( mockItem );

            Assert.AreSame( mockItem, character.UnequipItem( p_handSlot ) );
        }

        [TestCase( ItemSlot.LeftHand )]
        [TestCase( ItemSlot.RightHand )]
        public void UnqeuipItem_OnEmptySlot_ReturnsNull( ItemSlot p_handSlot )
        {
            ICharacter character = new Character( new Inventory(), new FakeJobBuilder( null ) );

            IItem item = character.UnequipItem( p_handSlot );

            Assert.IsNull( item );
        }

        [TestCase( ItemSlot.LeftHand )]
        [TestCase( ItemSlot.RightHand )]
        public void UnequipItem_RemovesItemFromList_ReturnsLessThanPrevious( ItemSlot p_handSlot )
        {
            ICharacter character = new Character( new Inventory(), new FakeJobBuilder( null ) );
            IItem stubItem = new FakeItem { ItemSlot = p_handSlot };

            character.EquipItem( stubItem );
            
            int countAfterEquip = character.Inventory.Items.Count;
            
            character.UnequipItem( p_handSlot );
            int countAfterUnequip = character.Inventory.Items.Count;

            Assert.Less( countAfterUnequip, countAfterEquip );
        }
    }
}
