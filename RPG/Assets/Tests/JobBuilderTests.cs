﻿using NUnit.Framework;

namespace Tests
{
    public class JobBuilderTests
    {
        [Test]
        public void BuildJob_WithNoItem_ReturnsNull()
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            character.BuildJob();

            Assert.IsNull( character.Job );
        }

        [TestCase( ItemType.Shield, ItemSlot.LeftHand )]
        [TestCase( ItemType.Sword, ItemSlot.RightHand )]
        public void BuildJob_WithOneItem_ReturnsNotNull( ItemType p_itemType, ItemSlot p_handSlot )
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem stubItem = new FakeItem { ItemType = p_itemType, ItemSlot = p_handSlot };
            character.EquipItem( stubItem );
            character.BuildJob();

            Assert.NotNull( character.Job );
        }

        [Test]
        public void BuildJob_WithItemsSwordAdnShield_ReturnsPaladin()
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem sword = new Sword();
            character.EquipItem( sword );

            IItem shield = new Shield();
            character.EquipItem( shield );

            character.BuildJob();

            Assert.IsTrue( character.Job is Paladin );
        }

        [Test]
        public void BuildJob_WithItemsStaffAndShield_ReturnsCleric()
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem staff = new Staff();
            character.EquipItem( staff );

            IItem shield = new Shield();
            character.EquipItem( shield );

            character.BuildJob();

            Assert.IsTrue( character.Job is Cleric );
        }

        [Test]
        public void BuildJob_WithItemsBowAndDagger_ReturnsRanger()
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem bow = new Bow();
            character.EquipItem( bow );

            IItem dagger = new Dagger();
            character.EquipItem( dagger );

            character.BuildJob();

            Assert.IsTrue( character.Job is Ranger );
        }

        [Test]
        public void BuildJob_WithItemAxe_ReturnsBarbarian()
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem axe = new Axe();
            character.EquipItem( axe );

            character.BuildJob();

            Assert.IsTrue( character.Job is Barbarian );
        }

        [Test]
        public void BuildJob_WithItemStaffAndSpellbook_ReturnsWizard()
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem staff = new Staff();
            character.EquipItem( staff );

            IItem spellbook = new Spellbook();
            character.EquipItem( spellbook );

            character.BuildJob();

            Assert.IsTrue( character.Job is Wizard );
        }

        // Right Hand | Left Hand
        [TestCase( ItemType.Sword, ItemType.Spellbook )]
        [TestCase( ItemType.Sword, ItemType.Bow )]
        [TestCase( ItemType.Sword, ItemType.None )]
        [TestCase( ItemType.Staff, ItemType.Bow )]
        [TestCase( ItemType.Staff, ItemType.None )]
        [TestCase( ItemType.Axe, ItemType.Bow )]
        [TestCase( ItemType.Axe, ItemType.Spellbook )]
        [TestCase( ItemType.Axe, ItemType.Shield )]
        [TestCase( ItemType.Dagger, ItemType.Spellbook )]
        [TestCase( ItemType.Dagger, ItemType.Shield )]
        [TestCase( ItemType.Dagger, ItemType.None )]
        public void BuildJob_WithItemUnsupportedCombination_ReturnsNovice( ItemType p_itemType1, ItemType p_itemType2 )
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem stubItem1 = new FakeItem{ ItemType = p_itemType1, ItemSlot = ItemSlot.RightHand };
            character.EquipItem( stubItem1 );

            IItem stubItem2 = new FakeItem{ ItemType = p_itemType2, ItemSlot = ItemSlot.LeftHand };
            character.EquipItem( stubItem2 );

            character.BuildJob();

            Assert.IsTrue( character.Job is Novice );
        }

        // Right Hand | Left Hand
        [TestCase( ItemType.Sword, ItemType.Shield )]
        [TestCase( ItemType.Staff, ItemType.Shield )]
        [TestCase( ItemType.Dagger, ItemType.Bow )]
        // [TestCase( ItemType.Axe, ItemType.None )] // We didn't include Barbarian since it only has one item
        [TestCase( ItemType.Staff, ItemType.Spellbook )]
        public void BuildJob_AfterUnequippingRightHandItem_RetrunsDifferentClass( ItemType p_itemType1, ItemType p_itemType2 )
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem stubItem1 = new FakeItem{ ItemType = p_itemType1, ItemSlot = ItemSlot.RightHand };
            character.EquipItem( stubItem1 );

            IItem stubItem2 = new FakeItem{ ItemType = p_itemType2, ItemSlot = ItemSlot.LeftHand };
            character.EquipItem( stubItem2 );

            character.BuildJob();

            IJob previousJob = character.Job;

            character.UnequipItem( ItemSlot.RightHand );

            character.BuildJob();

            IJob newJob = character.Job;

            Assert.AreNotEqual( previousJob.Name, newJob.Name );
        }

        [TestCase( ItemType.Sword, ItemType.Shield )]
        [TestCase( ItemType.Staff, ItemType.Shield )]
        [TestCase( ItemType.Dagger, ItemType.Bow )]
        // [TestCase( ItemType.Axe, ItemType.None )] // We didn't include Barbarian since it only has one item
        [TestCase( ItemType.Staff, ItemType.Spellbook )]
        public void BuildJob_AfterUnequippingLeftHandItem_RetrunsDifferentClass( ItemType p_itemType1, ItemType p_itemType2 )
        {
            Character character = new Character( new Inventory(), new JobBuilder() );
            IItem stubItem1 = new FakeItem{ ItemType = p_itemType1, ItemSlot = ItemSlot.RightHand };
            character.EquipItem( stubItem1 );

            IItem stubItem2 = new FakeItem{ ItemType = p_itemType2, ItemSlot = ItemSlot.LeftHand };
            character.EquipItem( stubItem2 );

            character.BuildJob();

            IJob previousJob = character.Job;

            character.UnequipItem( ItemSlot.LeftHand );

            character.BuildJob();

            IJob newJob = character.Job;

             Assert.AreNotEqual( previousJob.Name, newJob.Name );
        }
    }
}
